package com.eafit.flightattendant.flightprogrammer.repositories;

import java.util.ArrayList;
import java.util.List;

import com.eafit.flightattendant.flightprogrammer.domain.Empresa;
import com.eafit.flightattendant.flightprogrammer.domain.MedioTransporte;

public class InMemoryEmpresaRepository implements EmpresaRepository {
	
	private List<Empresa> empresas = new ArrayList<Empresa>();
	
	public InMemoryEmpresaRepository() {
		Empresa empresa = new Empresa();
		empresa.setId("1");
		empresa.setNombre("Empresa 1");
		MedioTransporte medio = new MedioTransporte("HK1101", 300);
		empresa.addMedioTrasporte(medio);
		empresas.add(empresa);
		
	}	
	
	@Override
	public void save(Empresa empresa) {
		empresas.add(empresa);
		
	}

	@Override
	public Empresa load(String id) {
		for (Empresa empresa : empresas) {
			if(empresa.getId().equals(id)){
				return empresa;
			}
		}
		return null;
	}

}
