package com.eafit.flightattendant.flightprogrammer.repositories;

import java.util.HashMap;
import java.util.Map;

import com.eafit.flightattendant.flightprogrammer.domain.Estacion;

public class InMemoryEstacionRepository implements EstacionRepository {

	private Map<String, Estacion> estaciones = new HashMap<String, Estacion>();

	public InMemoryEstacionRepository() {
		Estacion medellin = new Estacion("MDE", "MEDELLIN");
		Estacion bogota = new Estacion("BTA", "BOGOTA");
		estaciones.put(medellin.getCodigo(), medellin);
		estaciones.put(bogota.getCodigo(), bogota);
	}
	
	@Override
	public void save(Estacion estacion) {
		estaciones.put(estacion.getCodigo(), estacion);
	}

	@Override
	public Estacion get(String codigo) {
		return estaciones.get(codigo);
	}

}
