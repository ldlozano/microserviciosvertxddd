package com.eafit.flightattendant.flightprogrammer.repositories;

import com.eafit.flightattendant.flightprogrammer.domain.Estacion;

public interface EstacionRepository {
	void save(Estacion estacion);
	Estacion get(String codigo);
}	
