package com.eafit.flightattendant.flightprogrammer.repositories;

import com.eafit.flightattendant.flightprogrammer.domain.Empresa;

public interface EmpresaRepository {
	void save(Empresa empresa);
	Empresa load(String id);
}
