package com.eafit.flightattendant.flightprogrammer.domain;

public class MedioTransporte {
	private String identificacion;
	private int capacidad;

	public MedioTransporte(String identificacion, int capacidad) {
		this.identificacion = identificacion;
		this.capacidad = capacidad;
	}

	public String getIdentificacion() {
		return identificacion;
	}
	public int getCapacidad() {
		return capacidad;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		MedioTransporte comparado = (MedioTransporte) obj;
		if(comparado.getIdentificacion().equals(this.identificacion) 
				&& comparado.getCapacidad() == this.capacidad){
			return true;
		}
		return false;
	}
	
}
