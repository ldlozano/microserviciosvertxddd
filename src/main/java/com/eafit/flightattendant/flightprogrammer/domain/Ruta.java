package com.eafit.flightattendant.flightprogrammer.domain;

public class Ruta {
	private String nombre;
	private Estacion origen;
	private Estacion destino;
	private Empresa encargado;
	private String horario;
	private MedioTransporte trasporte;

	public Ruta(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public Estacion getOrigen() {
		return origen;
	}

	public Estacion getDestino() {
		return destino;
	}

	public Empresa getEncargado() {
		return encargado;
	}

	public String getHorario() {
		return horario;
	}

	public MedioTransporte getTrasporte() {
		return trasporte;
	}

	public void setTrasporte(MedioTransporte trasporte) {
		this.trasporte = trasporte;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setOrigen(Estacion origen) {
		this.origen = origen;
	}

	public void setDestino(Estacion destino) {
		this.destino = destino;
	}

	public void setEncargado(Empresa encargado) {
		this.encargado = encargado;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	@Override
	public boolean equals(Object obj) {
		Ruta comparado = (Ruta) obj;
		if (comparado == null)
			return false;

		if (!comparado.getNombre().equals(this.nombre)) {
			return false;
		}
		return true;
	}

}
