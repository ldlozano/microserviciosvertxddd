package com.eafit.flightattendant.flightprogrammer.domain;

public class Estacion {
	private String codigo;
	private String nombreCiudad;
	
	public Estacion(String codigo, String nombreCiudad) {
		this.codigo = codigo;
		this.nombreCiudad = nombreCiudad;
	}

	public String getCodigo() {
		return codigo;
	}
	public String getNombreCiudad() {
		return nombreCiudad;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		Estacion comparado = (Estacion) obj;
		if(comparado.getCodigo().equals(this.codigo)){
			return true;
		}
		return false;
	}
	
}
