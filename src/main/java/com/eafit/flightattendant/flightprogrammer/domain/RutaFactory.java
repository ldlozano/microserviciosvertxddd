package com.eafit.flightattendant.flightprogrammer.domain;

public class RutaFactory {

	public static Ruta create(Empresa empresa, String nombreRuta, Estacion estacionOrigen, Estacion estacionDestino,
			String horario, MedioTransporte medioTransporte) {
		Ruta ruta = new Ruta(nombreRuta);		
		ruta.setOrigen(estacionOrigen);
		ruta.setDestino(estacionDestino);
		ruta.setHorario(horario);
		ruta.setTrasporte(medioTransporte);
		ruta.setEncargado(empresa);
		return ruta;
	}

}
