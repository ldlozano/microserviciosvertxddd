package com.eafit.flightattendant.flightprogrammer.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Empresa {
	private String id;
	private String nombre;
	private List<Ruta> rutasProgramadas;
	private List<MedioTransporte> mediosTransporte;
	
	public Empresa() {
		UUID uuid = UUID.randomUUID();
		this.setId(uuid.toString());
		this.rutasProgramadas = new ArrayList<Ruta>();
		this.mediosTransporte = new ArrayList<MedioTransporte>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Ruta> getRutasProgramadas() {
		return rutasProgramadas;
	}

	public void setRutasProgramadas(List<Ruta> rutasProgramadas) {
		this.rutasProgramadas = rutasProgramadas;
	}
	
	public void adicionarRuta(Ruta ruta){
		rutasProgramadas.add(ruta);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		Empresa comparado = (Empresa) obj;
		if(comparado.getId().equals(this.getId())){
			return true;
		}
		return false;
	}

	public MedioTransporte getMedioTransporte(String idMedioTransporte) {
		MedioTransporte medio = null;
		for (MedioTransporte medioTransporte : mediosTransporte) {
			if(medioTransporte.getIdentificacion().equals(idMedioTransporte)){
				medio = medioTransporte;
			}
		}
		return medio;
	}

	public List<MedioTransporte> getMediosTransporte() {
		return mediosTransporte;
	}

	public void setMediosTransporte(List<MedioTransporte> mediosTransporte) {
		this.mediosTransporte = mediosTransporte;
	}

	public void addMedioTrasporte(MedioTransporte medio) {
		this.mediosTransporte.add(medio);
	}
	
}
