package com.eafit.flightattendant.flightprogrammer.service;

import com.eafit.flightattendant.flightprogrammer.application.ProgramacionRutaServiceImpl;
import com.eafit.flightattendant.flightprogrammer.application.ProgramacionRutasService;
import com.eafit.flightattendant.flightprogrammer.domain.Empresa;
import com.eafit.flightattendant.flightprogrammer.repositories.InMemoryEmpresaRepository;
import com.eafit.flightattendant.flightprogrammer.repositories.InMemoryEstacionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class RouteProgrammerRestService extends AbstractVerticle {

	ProgramacionRutasService programacionService;

	@Override
	public void start(Future<Void> future) throws Exception {
		programacionService = new ProgramacionRutaServiceImpl();
		programacionService.setEmpresaRepository(new InMemoryEmpresaRepository());
		programacionService.setEstacionRepository(new InMemoryEstacionRepository());
		
		Router router = Router.router(vertx);

		router.route().handler(BodyHandler.create());
		router.put("/company/:idCompany/routes").handler(this::handleAddFlight);
		router.get("/company/:idCompany/routes").handler(this::handleListFlights);

		vertx.createHttpServer().requestHandler(router::accept).listen(8080, resultado -> {
			if (resultado.succeeded()) {
				future.complete();
			} else {
				future.fail(resultado.cause());
			}
		});

	}

	private void handleAddFlight(RoutingContext routingContext) {
		String idCompany = routingContext.request().getParam("idCompany");
		HttpServerResponse response = routingContext.response();
		if (idCompany == null) {
			sendError(400, response);
		} else {
			JsonObject data = routingContext.getBodyAsJson();
			if (data == null) {
				sendError(400, response);
			} else {
				programacionService.adicionarRuta(idCompany, data.getString("ruta"),
						data.getString("origen"), data.getString("destino"),
						data.getString("horario"),data.getString("medio"));
				response.end();
			}
		}

	}

	private void handleListFlights(RoutingContext routingContext) {
		String idCompany = routingContext.request().getParam("idCompany");
		HttpServerResponse response = routingContext.response();
		if (idCompany == null) {
			sendError(400, response);
		} else {
			
			Empresa company = programacionService.obtenerEmpresa(idCompany);
			ObjectMapper mapper = new ObjectMapper();
			String responseObject = "{}";
			try {
				responseObject = mapper.writeValueAsString(company);
			} catch (JsonProcessingException e) {
				sendError(500, response);
			}
			if (company == null) {
				sendError(404, response);
			} else {
				response.putHeader("content-type", "application/json").end(responseObject);
			}
		}
	}

	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

}
