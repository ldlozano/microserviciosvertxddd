package com.eafit.flightattendant.flightprogrammer.application;

import com.eafit.flightattendant.flightprogrammer.domain.Empresa;
import com.eafit.flightattendant.flightprogrammer.domain.Estacion;
import com.eafit.flightattendant.flightprogrammer.domain.MedioTransporte;
import com.eafit.flightattendant.flightprogrammer.domain.Ruta;
import com.eafit.flightattendant.flightprogrammer.domain.RutaFactory;
import com.eafit.flightattendant.flightprogrammer.repositories.EmpresaRepository;
import com.eafit.flightattendant.flightprogrammer.repositories.EstacionRepository;

public class ProgramacionRutaServiceImpl implements ProgramacionRutasService {

	EmpresaRepository empresaRepository;
	EstacionRepository estacionRepository;
	
	@Override
	public Empresa obtenerEmpresa(String empresaId) {
		Empresa empresa = empresaRepository.load(empresaId);
		return empresa;
	}

	@Override
	public Ruta adicionarRuta(String empresaId, String nombreRuta, String estacionOrigen, String estacionDestino, String horario, String idMedioTransporte) {
		Empresa empresa = obtenerEmpresa(empresaId);
		Estacion origen = estacionRepository.get(estacionOrigen);
		Estacion destino = estacionRepository.get(estacionDestino);
		MedioTransporte medioTransporte = empresa.getMedioTransporte(idMedioTransporte);
		Ruta ruta = RutaFactory.create(empresa, nombreRuta, origen, destino, horario, medioTransporte);
		empresa.adicionarRuta(ruta);
		empresaRepository.save(empresa);
		return ruta;
	}

	@Override
	public void setEmpresaRepository(EmpresaRepository repo) {
		this.empresaRepository = repo;
	}

	@Override
	public void setEstacionRepository(EstacionRepository repo) {
		this.estacionRepository = repo;
	}

}
