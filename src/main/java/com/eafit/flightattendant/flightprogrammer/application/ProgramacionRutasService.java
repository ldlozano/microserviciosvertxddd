package com.eafit.flightattendant.flightprogrammer.application;

import com.eafit.flightattendant.flightprogrammer.domain.Empresa;
import com.eafit.flightattendant.flightprogrammer.domain.Ruta;
import com.eafit.flightattendant.flightprogrammer.repositories.EmpresaRepository;
import com.eafit.flightattendant.flightprogrammer.repositories.EstacionRepository;

public interface ProgramacionRutasService {
	Empresa obtenerEmpresa(String empresaId);
	Ruta adicionarRuta(String empresaId, String nombreRuta, String estacionOrigen, String estacionDestino, String horario, String idMedioTransporte);
	void setEmpresaRepository(EmpresaRepository repo);
	void setEstacionRepository(EstacionRepository repo);
}
